import React from "react";
const Navbar = () => {
  return (
    <nav className="Navbar">
      <div>
        <div className="Navbar_Container">
          <img
            src="https://cdn.logojoy.com/wp-content/uploads/2018/05/30164225/572-768x591.png"
            alt=""
          ></img>
          <div className="Navbar_Links">
            <ul>
              <li>Our Story</li>
              <li>Membership</li>
              <li>Write</li>
              <li>Sign In</li>
              <li>
                <button>Get Started</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
